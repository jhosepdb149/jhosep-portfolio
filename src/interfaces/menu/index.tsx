import React from 'react';

export interface IconsPropsModel {
  icon: React.ReactNode;
  text: string;
  toRoute?: () => void;
  active?: boolean;
}
