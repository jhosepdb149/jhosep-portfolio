import React from 'react';
import { CssBaseline,
  ThemeProvider } from '@wulperstudio/cms/node_modules/@mui/material';

import defaultTheme from './styles/defaultTheme';
import { AppRouter } from './routers/AppRouter';

function App() {
  return (
    <>
      <ThemeProvider theme={defaultTheme}>
        <CssBaseline />
        <AppRouter />
      </ThemeProvider>
    </>
  );
}

export default App;
