import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { APP_ROUTES } from './routes';
import { HomeRoutes } from './HomeRoutes';

export const AppRouter: React.FC = () => (
  <>
    <BrowserRouter>
      <Routes>
        <Route path={APP_ROUTES.home} element={<HomeRoutes />} />
      </Routes>
    </BrowserRouter>
  </>
);
