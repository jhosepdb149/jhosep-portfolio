import React from 'react';
import { Icon } from '@iconify/react';
import { LayoutMenuContent } from '@wulperstudio/cms';
import { useLocation, Routes, Route } from 'react-router-dom';

import { APP_ROUTES } from './routes';
import { HomePage } from '../pages/Home';
import { MenuBase } from '../components/Menu';
import { IconsPropsModel } from '../interfaces/menu';

export const HomeRoutes: React.FC = () => {
  const location = useLocation();

  const OPTIONS_MENU_NAV: Array<IconsPropsModel> = [
    {
      icon: <Icon icon="ant-design:home-outlined" width="24" height="24" />,
      text: 'Home',
      active: location.pathname === APP_ROUTES.home,
    },
  ];

  return (
    <>
      <LayoutMenuContent
        menu={<MenuBase icons={OPTIONS_MENU_NAV} />}
        content={(
          <Routes>
            <Route path={APP_ROUTES.home} element={<HomePage />} />
          </Routes>
        )}
      />
    </>
  );
};
