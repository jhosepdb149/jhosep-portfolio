import React from 'react';
import { Menu } from '@wulperstudio/cms';

import { IconsPropsModel } from '../../interfaces/menu';

export interface MenuBasePropsModel {
  icons: IconsPropsModel[];
}

export const MenuBase: React.FC<MenuBasePropsModel> = ({ icons }) => (
  <Menu
    menu="float"
    display="flex"
    icons={icons}
    logoUrl="https://source.unsplash.com/random"
    order={2}
    spacing={3}
    spacingIcons={2}
  />
);
